# ENGSCI233: Lab - Quality Control
# qclab_functions.py

# PURPOSE:
# To WRITE docstring specifications and IMPLEMENT precondition checking.

# PREPARATION:
# Notebook quality_control.ipynb.

# SUBMISSION:
# - YOU MUST submit this file

# TO DO:
# - COPY-PASTE your code from errlab to complete functions lu_factor(), lu_forward_sub(), lu_backward_sub()
# - COMPLETE the docstrings for lu_forward_sub() and lu_backward_sub().
# - COMPLETE the precondition check isSquare().
# - IMPLEMENT precondition checking in lu_factor(), lu_forward_sub() and lu_backward_sub().
# - TEST your code is working correctly by passing the asserts in qclab_tasks.py.
# - DO NOT modify the other functions.

import numpy as np
from copy import copy
from warnings import warn

# this function is complete
def lu_read(filename):	
	''' 
	Load cofficients of a linear system from a file.
	
	Parameters
	----------
	filename : str
		Name of file containing A and b.
		
	Returns
	-------
	A : np.array
		Matrix for factorisation.
	b : np.array
		RHS vector.
		
	Notes
	-----
	filename should point to a text file 
	
	Examples
	--------
	The syntax for a determined linear system with four unknowns in the text file. 
	1x1 scalar for unknowns (row 0)
	4x4 matrix for coefficients (rows 1 to 4)
	1x4 matrix for RHS vector (row 5)

	4 
	 2.0  3.0 -4.0  2.0
	-4.0 -5.0  6.0 -3.0
	 2.0  2.0  1.0  0.0
	-6.0 -7.0 14.0 -4.0
	 4.0 -8.0  9.0  6.0
	'''
	
	with open(filename, 'r') as fp:
		# Row dimension
		nums = fp.readline().strip()
		row = int(nums)
		
		A = []
		for i in range(row):
			nums = fp.readline().rstrip().split()
			A.append([float(num) for num in nums])
		A = np.array(A)
		
		b = []
		nums = fp.readline().rstrip().split()
		b.append([float(num) for num in nums])
		b = np.array(b)
		
	return A, b.T

	
# **this function is incomplete**
#					 ----------
def lu_factor(A, pivot=False):
	"""
	Return LU factors and row swap vector p of matrix A.
	
	Parameters
	----------
	A : np.array
		Matrix for factorisation. Must be square.
	pivot : Boolean
		Use partial pivoting.
			
	Returns
	-------
	A : np.array
		Condensed LU factors
	p : np.array
		Row swap vector

	Notes
	-----
	Factorisation occurs in place, i.e., input matrix A is permanently 
	modified by the function. L and U factors are stored in lower and upper 
	triangular parts of the output.
	
	Examples
	--------
	>>>A = np.array([
	[ 2, 3,-4, 2],
	[-4,-5, 6,-3],
	[ 2, 2, 1, 0],
	[-6,-7,14,-4]])
	>>>lu_factor(A, pivot=False)
	np.array([
	[ 2, 3,-4, 2],
	[-2, 1,-2, 1],
	[ 1,-1, 3,-1],
	[-3, 2, 2, 2]])
	"""
	
	# **this needs to be completed***
	# Precondition (check if a matrix is square)
	# hint: you can use isSquare() function
	#
	# if *condition*
	#	raise 'Matrix L is not square'
	if (not isSquare(A)):
			raise ValueError
	# get dimensions of square matrix 
	n = np.shape(A)[0] 	
	
	# create initial row swap vector: p = [0, 1, 2, ... n]
	p = np.arange(n) 		

	# loop over each row in the matrix
	for i in range(n):		

		# Step 2: Row swaps for partial pivoting
		if pivot:
				# Initialising maximum column
				A_iMax = abs(A[i,i])
				pMax = i
				for j in range(i+1,n): # Checking the remaining rows for larger entries
						if (abs(A[j,i]) > A_iMax):
								# Storing new max value
								A_iMax = abs(A[j,i])
								pMax = j
				# Swapping rows and storing info in p
				temp = copy(A[i])
				A[i] = A[pMax]
				A[pMax] = temp
				p[i] = pMax

		# Step 0: Get the pivot value
		# Step 1: Perform the row reduction operations 			 	
		for j in range(i+1,n):
			L_ji = A[j,i]/A[i,i] # Using the pivot value to find L_ji
			A[j][i:] -= A[i][i:]*L_ji # Manipulating A without altering L to get closer to U
			A[j,i] = L_ji # Placing L_ji back in A to store L
	
	return A, p

	
# **this function is incomplete**
#					 ----------
def lu_forward_sub(L, b, p=None):
	"""
	Return solution y for Ly = pb, with optional row swap vector for b
	
	Parameters
	----------
	L : np.array
		Matrix containing 'L' of an LU factorisation as its lower triangle
	p : np.array
		Row swap vector
	b : np.array
		Vector to be solved against
			
	Returns
	-------
	b : np.array
		Solution vector

	Notes
	-----
	Solution occurs in place, i.e., input vector is permanently 
	modified by the function. L need not be triangular as a whole, just contain
	the values of L as its lower triangle.

	Examples
	--------
	>>>L = np.array([
	[ 1, 0, 0, 0],
	[-2, 1, 0, 0],
	[ 1,-1, 1, 0],
	[-3, 2, 2, 1]])
	>>>b = np.array([
    [1],
    [2],
    [3],
    [4]])
	>>>y = lu_forward_sub(L,b)
	np.array([
    [1],
    [4],
    [6],
    [-13]])
	"""	
	
	# check shape of L consistent with shape of b (for matrix multiplication L^T*b)
	assert np.shape(L)[0] == len(b), 'incompatible dimensions of L and b'
	
	# Step 0: Get matrix dimension										
	n = np.shape(L)[0] 	

	# Step 2: Perform partial pivoting row swaps on RHS
	if p is not None:
		for i in range(n): # Using p to permutate b to be compatible with the factorisation
				temp = copy(b[i])
				b[i] = b[p[i]]
				b[p[i]] = temp
	
	# Step 1: Perform forward substitution operations	
	for i in range(1,n): # Since L is lower triangular, we can just multiply the row by B and store the values of y back
			b[i] -= np.dot(L[i][:i],b[:i]) # in it, as it only uses b_i and the known values of y for the calculation

	return b

	
# **this function is incomplete**
#					 ----------
def lu_backward_sub(U, y):
	"""
	Return solution x for Ux = y.
	
	Parameters
	----------
	U : np.array
		Matrix containing 'U' of an LU factorisation as its upper triangle
	y : np.array
		Vector to be solved against
			
	Returns
	-------
	y : np.array
		Solution vector

	Notes
	-----
	Solution occurs in place, i.e., input vector is permanently 
	modified by the function. U need not be triangular as a whole, just contain
	the values of U as its upper triangle.

	Examples
	--------
	>>>U = np.array([
	[ 2, 3,-4, 2],
	[ 0, 1,-2, 1],
	[ 0, 0, 3,-1],
	[ 0, 0, 0, 2]])	
	>>>y = np.array([
    [1],
    [4],
    [6],
    [-13]])
	>>>x = lu_backward_sub(U,y)
	np.array([
	[-8],
	[10],
	[0],
	[-6]])
	"""

	# check shape consistency
	assert np.shape(U)[0] == len(y), 'incompatible dimensions of U and y'

	# Perform backward substitution operations
	for i in range (len(y)-1,-1,-1): # Back substituting and storing values in y, as only x[i+1:] is needed and y[i] indexed at any time
				y[i] = (y[i] - np.dot(U[i][i+1:],y[i+1:]) ) / U[i,i]

	# Return 
	return y											

	
# **this function is incomplete**
#					 ----------
def isSquare(A):
	""" Check whether a matrix is square (NxN)
		
	Parameters
	----------
	A : np.array
		Matrix to be checked
		
	Returns
	-------
	B : Boolean
		True for square matrix, false if not
	
	Notes
	-----
	A should be a numpy array
	
	Examples
	--------
	>>>A = np.array([
	[2, 1],
	[17,4]])
	>>>isSquare (A)
	True
	"""
	# **this needs to be completed***	
	# check that the condition of a square matrix is satisfied
	y = A.shape
	B = (y[0] == y[1]) # y is a vector [#rows, #cols]
	return B
