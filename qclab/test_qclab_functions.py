# ENGSCI233: Lab - Quality Control
# test_qclab_tasks.py

from qclab_functions import *
import numpy  as np
from numpy.linalg import norm

# test LU factorization function (w/o partial pivot)
# **this function is incomplete**
tol = 1.e-10
def test_lu_factor():
	"""
	Test if function lu_factor is working properly by comparing it with a known result.

	Tests should be expanded to include two different matrices, as well as partial pivoting functionality.
	"""
	#[A, b] = lu_read('test1.txt')
	# it is poor form to read an external file into a test function, as above
	A = np.array([
		[ 2.,  3., -4.,  2.],
		[-4., -5.,  6., -3.],
		[ 2.,  2.,  1.,  0.],
		[-6., -7., 14., -4.]])	
	LU,p = lu_factor(A, pivot=False)
	LU_soln = np.array([
		[ 2, 3,-4, 2],
		[-2, 1,-2, 1],
		[ 1,-1, 3,-1],
		[-3, 2, 2, 2]])	
	assert norm(LU - LU_soln) < tol

	# testing different matrix without pivoting
	A = np.array([
		[ 3.,-7.,-2., 2.],
		[-3., 5., 1., 0.],
		[ 6.,-4., 0.,-5.],
		[-9., 5.,-5.,12.]])
	LU,p = lu_factor(A, pivot=False)
	LU_soln = np.array([
		[ 3,-7,-2, 2],
		[-1,-2,-1, 2],
		[ 2,-5,-1, 1],
		[-3, 8, 3,-1]])
	assert norm(LU - LU_soln) < tol

	# test LU factorization function (w partial pivot)
	# **write your function here**
	A = np.array([
		[ 2., 1., 1., 0.],
		[ 4., 3., 3., 1.],
		[ 8., 7., 9., 5.],
		[ 6., 7., 9., 8.]])
	LU,p = lu_factor(A, pivot=True)
	LU_soln = np.array([
		[ 8, 7, 9, 5],
		[3/4,7/4,9/4,17/4],
		[1/2,-2/7,-6/7,-2/7],
		[1/4,-3/7,1/3,2/3]])
	assert norm(LU - LU_soln) < tol

	# testing 1x1 matrix
	A = np.array([
		[1]])
	LU,p = lu_factor(A, pivot=False)
	LU_soln = np.array([
		[1]])
	assert norm(LU - LU_soln) < tol

	# testing a 1 by 1 matrix of zeros
	A = np.array([
		[0]])
	LU,p = lu_factor(A, pivot=False)
	LU_soln = np.array([
		[0]])
	assert norm(LU - LU_soln) < tol

	# testing a 1x1 matrix with pivoting
	A = np.array([
		[1]])
	LU,p = lu_factor(A, pivot=True)
	LU_soln = np.array([
		[1]])
	assert norm(LU - LU_soln) < tol

	# testing a 1x1 matrix of zeros with pivoting
	A = np.array([
		[0]])
	LU,p = lu_factor(A, pivot=True)
	LU_soln = np.array([
		[0]])
	assert norm(LU - LU_soln) < tol

	# testing a nonsquare matrix
	A = np.array([
		[ 1, 1]])
	try:
		lu_factor(A, pivot=False)
	except ValueError:
		pass

	# testing a very large matrix
	A = np.eye(1000)
	LU,p = lu_factor(A, pivot=False)
	assert norm(LU - A) < tol



# test forward substitution function
# **this function is incomplete**
def test_lu_forward_sub():
	""" *you don't need to write docstrings for these functions*
	"""
	# testing with no pivoting, normal L
	L = np.array([
		[ 1, 0, 0, 0],
		[-2, 1, 0, 0],
		[ 1,-1, 1, 0],
		[-3, 2, 2, 1]])
	b = np.array([
    	[1],
    	[2],
    	[3],
    	[4]])
	y_soln = np.array([
    	[1],
    	[4],
    	[6],
    	[-13]])
	y = lu_forward_sub(L,b)
	assert norm(y - y_soln) < tol

	# testing with no pivoting, mixed LU as one matrix
	L = np.array([
		[ 3,-7,-2, 2],
		[-1,-2,-1, 2],
		[ 2,-5,-1, 1],
		[-3, 8, 3,-1]])
	b = np.array([
    	[1],
    	[0],
    	[1],
    	[0]])
	y_soln = np.array([
    	[1],
    	[1],
    	[4],
    	[-17]])
	y = lu_forward_sub(L,b)
	assert norm(y - y_soln) < tol

	# testing with pivoting, mixed LU as one matrix, noninteger solutions
	L = np.array([
		[ 8, 7, 9, 5],
		[3/4,7/4,9/4,17/4],
		[1/2,-2/7,-6/7,-2/7],
		[1/4,-3/7,1/3,2/3]])
	b = np.array([
    	[1.],
    	[1.],
    	[1.],
    	[1.]])
	p = np.array([
		[2],
		[3],
		[3],
		[3]])
	y_soln = np.array([
    	[1],
    	[1/4],
    	[4/7],
    	[2/3]])
	y = lu_forward_sub(L,b,p)
	assert norm(y - y_soln) < tol

	# Testing very small matrix
	L = np.array([
		[1]])
	b = np.array([
    	[1]])
	p = np.array([
		[0]])
	y_soln = np.array([
    	[1]])
	y = lu_forward_sub(L,b,p)
	assert norm(y - y_soln) < tol



# test backward substitution function
# **write your function here**
def test_lu_backward_sub():
    	
	# Testing with normal U
	U = np.array([
		[ 2, 3,-4, 2],
		[ 0, 1,-2, 1],
		[ 0, 0, 3,-1],
		[ 0, 0, 0, 2]])	
	y = np.array([
    	[1],
    	[4],
    	[6],
    	[-13]])
	x_soln = np.array([
		[-8],
		[10],
		[0],
		[-6]])
	x = lu_backward_sub(U,y)
	assert norm(x - x_soln) < tol

	# testing with mixed U
	U = np.array([
		[ 3,-7,-2, 2],
		[-1,-2,-1, 2],
		[ 2,-5,-1, 1],
		[-3, 8, 3,-1]])
	y = np.array([
    	[1],
    	[1],
    	[4],
    	[-17]])
	x_soln = np.array([
		[21],
		[10],
		[13],
		[17]])
	x = lu_backward_sub(U,y)
	assert norm(x - x_soln) < tol

	U = np.array([
		[ 8, 7, 9, 5],
		[3/4,7/4,9/4,17/4],
		[1/2,-2/7,-6/7,-2/7],
		[1/4,-3/7,1/3,2/3]])
	y = np.array([
    	[1],
    	[1/4],
    	[4/7],
    	[2/3]])
	x_soln = np.array([
		[3/2],
		[-1],
		[-1],
		[1]])
	x = lu_backward_sub(U,y)
	assert norm(x - x_soln) < tol

	# Testing very small matrix
	U = np.array([
		[1]])
	y = np.array([
    	[1]])
	x_soln = np.array([
		[1]])
	x = lu_backward_sub(U,y)
	assert norm(x - x_soln) < tol

# test isSquare function
# **write your function here**
def test_isSquare():
	assert isSquare(np.eye(1)) # 1x1 matrix
	assert isSquare(np.eye(1000)) # very large square matrix
	assert not isSquare(np.ones((1,5))) # nonsquare matrix
	A = np.array([[1,0,1,0]]) # nonsquare matrix
	assert not isSquare(A) 
	A = np.array([[0]]) # 1x1 matrix of zeros
	assert isSquare(A)
	A = np.array([[1,0]]) # short nonsquare row vector
	assert not isSquare(A)
	A = np.array([[1], [0]]) # short nonsquare column vector
	assert not isSquare(A)